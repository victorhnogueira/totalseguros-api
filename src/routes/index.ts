import { Router, Request, Response } from 'express'
import { auth } from '../app/middlewares/auth'
import authRouter from './auth'
import userRouter from './user'
import clientRouter from './client'
import newsRouter from './news'
import segurosRouter from './seguros'

const routes = Router()

routes.get('/', async (req: Request, res: Response) => {
  return res.status(200).json({ api: 'Total Seguros API' })
})

routes.use('/auth', authRouter)
routes.use('/client', clientRouter)
routes.use('/news', newsRouter)
routes.use('/seguros', segurosRouter)

routes.use(auth)

routes.use('/user', userRouter)

export default routes

import { Router } from 'express'
import UserController from '@src/app/controller/UserController'
import ClientController from '@src/app/controller/ClientController'

const userRouter = Router()

userRouter.get('/', UserController.getUserData)
userRouter.patch('/', UserController.updateUserData)

userRouter.get('/clients', ClientController.getAll)
userRouter.get('/clients/:clientId', ClientController.getById)

userRouter.post('/segurosauto', UserController.createSegurosAuto)
userRouter.patch('/segurosauto/:seguroId', UserController.createSegurosAuto)
userRouter.get('/segurosauto', UserController.getSegurosAuto)
userRouter.get('/segurosauto/:seguroId', UserController.getSegurosAutoById)

export default userRouter

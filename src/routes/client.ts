import { Router } from 'express'
import ClientController from '@src/app/controller/ClientController'
import { clientAuth } from '../app/middlewares/clientAuth'

const clientRouter = Router()

clientRouter.get('/', clientAuth, ClientController.getClientData)
clientRouter.patch('/', clientAuth, ClientController.updateClientData)

clientRouter.get('/segurosauto', clientAuth, ClientController.getSegurosAuto)
clientRouter.get(
  '/segurosauto/:seguroId',
  clientAuth,
  ClientController.getSegurosAutoById
)

export default clientRouter

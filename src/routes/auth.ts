import { Router } from 'express'
import AuthController from '@src/app/controller/AuthController'

const authRouter = Router()

authRouter.post('/user/login', AuthController.userLogin)
authRouter.post('/user/register', AuthController.userRegister)

authRouter.post('/client/login', AuthController.clientLogin)
authRouter.post('/client/register', AuthController.clientRegister)

export default authRouter

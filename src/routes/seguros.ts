import { Router } from 'express'
import SeguroController from '@src/app/controller/SeguroController'

const segurosRouter = Router()

segurosRouter.get('/', SeguroController.getAll)
segurosRouter.get('/:seguroId', SeguroController.getById)
segurosRouter.post('/', SeguroController.create)

export default segurosRouter

import { Router } from 'express'
import NewsController from '@src/app/controller/NewsController'

const newsRouter = Router()

newsRouter.get('/', NewsController.getAll)
newsRouter.get('/:newsId', NewsController.getById)
newsRouter.post('/', NewsController.create)

export default newsRouter

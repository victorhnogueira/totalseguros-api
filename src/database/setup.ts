import {
  getConnection,
  getConnectionManager,
  getConnectionOptions
} from 'typeorm'

export const connect = async (): Promise<void> => {
  const connectionManager = getConnectionManager()

  if (!connectionManager.has('default')) {
    const connectionOptions = await getConnectionOptions()
    connectionManager.create(connectionOptions)
  }

  const connection = connectionManager.get()
  await connection.connect()
}

export const close = async (): Promise<void> => {
  const connection = getConnection()
  await connection.close()
}

export const refreshMigrations = async (): Promise<void> => {
  const connection = getConnection()
  await connection.dropDatabase()
  await connection.runMigrations({ transaction: 'all' })
}

import { MigrationInterface, QueryRunner, Table } from 'typeorm'

export class CreateClientTable1620683863830 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'clients',
        columns: [
          {
            name: 'id',
            type: 'int',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'fullname',
            type: 'varchar'
          },
          {
            name: 'passhash',
            type: 'varchar'
          },
          {
            name: 'cpf_cnpj',
            type: 'varchar',
            isUnique: true
          },
          {
            name: 'email',
            type: 'varchar',
            isNullable: true,
            isUnique: true
          },
          {
            name: 'phone',
            type: 'varchar',
            isNullable: true
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'NOW()'
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'NOW()'
          }
        ]
      })
    )
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('clients')
  }
}

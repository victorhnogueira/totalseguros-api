import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey
} from 'typeorm'

//

export class CreateSeguroTable1620683863832 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'seguros_auto',
        columns: [
          {
            name: 'id',
            type: 'int',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'client_id',
            type: 'int'
          },
          {
            name: 'veiculo',
            type: 'varchar'
          },
          {
            name: 'placa',
            type: 'varchar'
          },
          {
            name: 'seguradora',
            type: 'varchar'
          },
          {
            name: 'central_de_atendimento',
            type: 'varchar'
          },
          {
            name: 'numero_apolice',
            type: 'varchar'
          },
          {
            name: 'franquia',
            type: 'varchar'
          },
          {
            name: 'danos_materiais',
            type: 'varchar'
          },
          {
            name: 'danos_corporais',
            type: 'varchar'
          },
          {
            name: 'danos_morais',
            type: 'varchar'
          },
          {
            name: 'carro_reserva',
            type: 'varchar'
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'NOW()'
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'NOW()'
          }
        ]
      })
    )

    await queryRunner.createForeignKey(
      'seguros_auto',
      new TableForeignKey({
        columnNames: ['client_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'clients'
      })
    )
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('seguros_auto')
  }
}

import {
  MigrationInterface,
  QueryRunner,
  TableForeignKey,
  Table
} from 'typeorm'

export class CreateNewsTable1621550868041 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'news',
        columns: [
          {
            name: 'id',
            type: 'int',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'user_id',
            type: 'int'
          },
          {
            name: 'title',
            type: 'varchar'
          },
          {
            name: 'content',
            type: 'varchar'
          },
          {
            name: 'image',
            type: 'varchar',
            isNullable: true
          },
          {
            name: 'cta',
            type: 'varchar',
            isNullable: true
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'NOW()'
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'NOW()'
          }
        ]
      })
    )

    await queryRunner.createForeignKey(
      'news',
      new TableForeignKey({
        columnNames: ['user_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'users'
      })
    )
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('news')
  }
}

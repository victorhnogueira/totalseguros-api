import express, { Application } from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import routes from './routes'
import * as database from './database/setup'
import * as Sentry from '@sentry/node'
import * as Tracing from '@sentry/tracing'
import config, { IConfig } from 'config'

const serverConfig: IConfig = config.get('App.server')

export class SetupServer {
  private app!: Application
  private PORT!: number

  constructor() {
    this.PORT = Number(process.env.PORT)
  }

  public async init(): Promise<void> {
    this.app = express()
    this.middleares()
    this.routes()
    await this.databaseSetup()

    // The error handler must be before any other error middleware and after all controllers
    this.app.use(Sentry.Handlers.errorHandler())
  }

  private middleares(): void {
    this.app.use(bodyParser.json())
    this.app.use(bodyParser.urlencoded({ extended: false }))
    this.app.use(cors())

    // Sentry setup
    Sentry.init({
      integrations: [
        // enable HTTP calls tracing
        new Sentry.Integrations.Http({ tracing: true }),
        // enable Express.js middleware tracing
        new Tracing.Integrations.Express({ app: this.app }),
        // enable PostgreSQL tracing
        new Tracing.Integrations.Postgres()
      ],

      // We recommend adjusting this value in production, or using tracesSampler
      // for finer control
      tracesSampleRate: 1.0
    })

    // RequestHandler creates a separate execution context using domains, so that every
    // transaction/span/breadcrumb is attached to its own Hub instance
    this.app.use(Sentry.Handlers.requestHandler())
    // TracingHandler creates a trace for every incoming request
    this.app.use(Sentry.Handlers.tracingHandler())
  }

  private routes(): void {
    this.app.use(routes)
  }

  private async databaseSetup(): Promise<void> {
    await database.connect()
  }

  public getApp(): Application {
    return this.app
  }

  public start(): void {
    this.app.listen(this.PORT, () => {
      console.log(`Server listening on port: ${this.PORT}`)
    })
  }

  public async close(): Promise<void> {
    await database.close()
  }
}

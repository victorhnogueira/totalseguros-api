import { Request, Response, NextFunction } from 'express'
import jwt from 'jsonwebtoken'
import config, { IConfig } from 'config'

export const auth = (
  req: Request,
  res: Response,
  next: NextFunction
): void | Response => {
  const authHeader = req.headers.authorization

  if (!authHeader) {
    return res.status(401).json({ error: 'Token is required' })
  }

  const [, token] = authHeader.split(' ')
  const jwtConfig: IConfig = config.get('App.JWTConfig')

  try {
    jwt.verify(token, String(jwtConfig.get('app_secret')))
    res.locals.jwtToken = token
    next()
  } catch (error) {
    return res.status(401).json({ error: 'token invalid' })
  }
}

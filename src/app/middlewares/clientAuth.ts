import { Request, Response, NextFunction } from 'express'
import jwt from 'jsonwebtoken'
import config, { IConfig } from 'config'

export const clientAuth = (
  req: Request,
  res: Response,
  next: NextFunction
): void | Response => {
  const authHeader = req.headers.authorization

  if (!authHeader) {
    return res.status(401).json({ error: 'client token is required' })
  }

  const [, token] = authHeader.split(' ')

  try {
    const jwtConfig: IConfig = config.get('App.JWTConfig')

    jwt.verify(token, String(jwtConfig.get('client_secret')))
    res.locals.clientToken = token
    next()
  } catch (error) {
    return res.status(401).json({ error: 'client token invalid' })
  }
}

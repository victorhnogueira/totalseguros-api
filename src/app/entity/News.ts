import { Entity, Column, ManyToOne, JoinColumn } from 'typeorm'
import CoreEntity from './CoreEntity'
import { IsString, IsOptional } from 'class-validator'
import User from './User'

@Entity('news')
export default class News extends CoreEntity {
  @Column({ name: 'user_id' })
  userId!: number

  @Column()
  @IsString()
  title?: string

  @Column()
  @IsString()
  content?: string

  @Column({ nullable: true })
  @IsOptional()
  @IsString()
  image?: string

  @Column({ nullable: true })
  @IsOptional()
  @IsString()
  cta?: string

  @ManyToOne(() => User, user => user.news)
  @JoinColumn({ name: 'user_id' })
  user!: User
}

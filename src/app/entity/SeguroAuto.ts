import { Entity, Column, ManyToOne, JoinColumn } from 'typeorm'
import CoreEntity from './CoreEntity'
import { IsString } from 'class-validator'
import Client from './Client'

@Entity('seguros_auto')
export default class SeguroAuto extends CoreEntity {
  @Column({ name: 'client_id' })
  clientId!: number

  @Column()
  @IsString()
  veiculo?: string

  @Column()
  @IsString()
  placa?: string

  @Column()
  @IsString()
  seguradora?: string

  @Column({ name: 'central_de_atendimento' })
  @IsString()
  centralDeAtendimento?: string

  @Column({ name: 'numero_apolice' })
  @IsString()
  numeroApolice?: string

  @Column()
  @IsString()
  franquia?: string

  @Column({ name: 'danos_materiais' })
  @IsString()
  danosMateriais?: string

  @Column({ name: 'danos_corporais' })
  @IsString()
  danosCorporais?: string

  @Column({ name: 'danos_morais' })
  @IsString()
  danosMorais?: string

  @Column({ name: 'carro_reserva' })
  @IsString()
  carroReserva?: string

  @ManyToOne(() => Client, client => client.seguroAuto)
  @JoinColumn({ name: 'client_id' })
  client!: Client
}

import { Entity, Column, OneToMany } from 'typeorm'
import CoreEntity from './CoreEntity'
import { IsEmail, IsNotEmpty, MaxLength } from 'class-validator'
import News from './News'

@Entity('users')
export default class User extends CoreEntity {
  @Column({ name: 'fullname' })
  @IsNotEmpty()
  @MaxLength(60)
  fullName!: string

  @Column({ select: false })
  passhash?: string

  @Column({ unique: true })
  @IsEmail()
  email!: string

  @OneToMany(() => News, news => news.user)
  news!: News[]
}

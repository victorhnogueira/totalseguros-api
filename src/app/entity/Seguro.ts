import { Entity, Column } from 'typeorm'
import CoreEntity from './CoreEntity'
import { IsString, IsOptional } from 'class-validator'

@Entity('seguros')
export default class Seguro extends CoreEntity {
  @Column()
  @IsString()
  title?: string

  @Column({ nullable: true })
  @IsString()
  @IsOptional()
  content?: string

  @Column({ nullable: true })
  @IsString()
  @IsOptional()
  cta?: string
}

import { Entity, Column, OneToMany } from 'typeorm'
import CoreEntity from './CoreEntity'
import {
  IsEmail,
  IsNotEmpty,
  MaxLength,
  IsString,
  IsOptional
} from 'class-validator'
import SeguroAuto from './SeguroAuto'

@Entity('clients')
export default class Client extends CoreEntity {
  @Column({ name: 'fullname' })
  @IsNotEmpty()
  @MaxLength(60)
  fullName!: string

  @Column({ select: false })
  passhash?: string

  @Column({ name: 'cpf_cnpj', unique: true })
  @IsString()
  cpfCnpj?: string

  @Column({ unique: true, nullable: true })
  @IsEmail()
  @IsOptional()
  email!: string

  @Column({ nullable: true })
  @IsString()
  @IsOptional()
  phone!: string

  @OneToMany(() => SeguroAuto, seguroAuto => seguroAuto.client)
  seguroAuto!: SeguroAuto[]
}

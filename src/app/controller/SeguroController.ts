import { Request, Response } from 'express'
import { getRepository } from 'typeorm'
import { validate } from 'class-validator'
import Seguro from '@entity/Seguro'

class SeguroController {
  async getAll(req: Request, res: Response) {
    const repository = getRepository(Seguro)
    const seguros = await repository.find()

    return res.status(200).json(seguros)
  }

  // ==============================================

  async getById(req: Request, res: Response) {
    const repository = getRepository(Seguro)
    const { seguroId } = req.params

    if (!seguroId) {
      return res.status(400).json({ code: 400, error: 'seguroId not provided' })
    }

    const seguro = await repository.findOne(seguroId)

    if (!seguro) {
      return res.status(400).json({ code: 400, error: 'seguro not found' })
    }

    return res.status(200).json(seguro)
  }

  // ==============================================

  async create(req: Request, res: Response) {
    const seguroRepository = getRepository(Seguro)
    const { title, content, cta } = req.body

    if (!title) {
      return res.status(400).json({ code: 400, error: 'title not provided' })
    }

    if (!cta) {
      return res.status(400).json({ code: 400, error: 'cta not provided' })
    }

    const seguro = seguroRepository.create({
      title,
      content,
      cta
    })

    const seguroErrors = await validate(seguro)

    if (seguroErrors.length >= 1) {
      return res.status(400).json({ code: 400, error: 'invalid fields' })
    }

    try {
      const saved = await seguroRepository.save(seguro)
      return res.status(200).json({ seguro: saved })
    } catch (error) {
      return res.status(400).json({ code: 400, error: 'error saving seguro' })
    }
  }
}

export default new SeguroController()

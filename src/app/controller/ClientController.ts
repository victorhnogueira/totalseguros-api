import { Request, Response } from 'express'
import { getRepository } from 'typeorm'
import jwt from 'jsonwebtoken'
import { validate } from 'class-validator'
import Client from '../entity/Client'
import SeguroAuto from '../entity/SeguroAuto'

interface JwtToken {
  id: number
  iat: number
  exp: number
}

type DecodedJwtToken = JwtToken | null

class ClientController {
  async getAll(req: Request, res: Response) {
    const repository = getRepository(Client)
    const clients = await repository.find()

    clients.map(client => delete client.passhash)

    return res.status(200).json(clients)
  }

  // ================================================================
  async getById(req: Request, res: Response) {
    const repository = getRepository(Client)
    const { clientId } = req.params

    if (!clientId) {
      return res.status(400).json({ error: 'clientId not provided' })
    }

    const client = await repository.findOne(clientId, {
      relations: ['seguroAuto']
    })

    if (!client) {
      return res.status(400).json({ error: 'client not found' })
    }

    return res.status(200).json(client)
  }

  // ================================================================

  async getClientData(_: Request, res: Response) {
    const clientRepository = getRepository(Client)

    const token = res.locals.clientToken

    if (!token) {
      return res.status(400).json({ error: 'Token not provided' })
    }

    const decodedJwt = jwt.decode(token) as DecodedJwtToken

    const client = await clientRepository
      .createQueryBuilder('client')
      .where('client.id = :id', { id: decodedJwt?.id })
      .getOne()

    if (!client) {
      return res.status(400).json({ error: 'client not found' })
    }

    return res.status(200).json(client)
  }

  // =================================================================

  async updateClientData(req: Request, res: Response) {
    const clientRepository = getRepository(Client)
    const { fullName, email } = req.body

    const token = res.locals.clientToken

    if (!token) {
      return res.status(400).json({ error: 'Token not provided' })
    }

    const decodedJwt = jwt.decode(token) as DecodedJwtToken

    const client = await clientRepository.findOne({
      where: {
        id: decodedJwt?.id
      }
    })

    if (!client) {
      return res.status(400).json({ error: 'client not found' })
    }

    if (fullName !== undefined) {
      client.fullName = fullName
    }

    if (email !== undefined) {
      client.email = email
    }

    const clientErrors = await validate(client)

    if (clientErrors.length >= 1) {
      return res.status(400).json(clientErrors)
    }

    try {
      const updatedClient = await clientRepository.save(client)
      return res.status(200).json(updatedClient)
    } catch (error) {
      return res.status(400).json(error)
    }
  }

  // =================================================================

  async getSegurosAuto(req: Request, res: Response) {
    const clientRepository = getRepository(Client)

    const token = res.locals.clientToken

    if (!token) {
      return res.status(400).json({ error: 'Token not provided' })
    }

    const decodedJwt = jwt.decode(token) as DecodedJwtToken

    const client = await clientRepository
      .createQueryBuilder('client')
      .where('client.id = :clientId', {
        clientId: decodedJwt?.id
      })
      .leftJoinAndSelect('client.seguroAuto', 'seguroAuto')
      .getOne()

    if (!client) {
      return res.status(400).json({ error: 'client not found' })
    }

    return res.status(200).json(client.seguroAuto)
  }

  // =================================================================

  async getSegurosAutoById(req: Request, res: Response) {
    const clientRepository = getRepository(Client)
    const seguroAutoRepository = getRepository(SeguroAuto)
    const { seguroId } = req.params

    const token = res.locals.clientToken

    if (!seguroId) {
      return res.status(400).json({ error: 'seguroId not provided' })
    }

    if (!token) {
      return res.status(400).json({ error: 'Token not provided' })
    }

    const decodedJwt = jwt.decode(token) as DecodedJwtToken

    const client = await clientRepository.findOne(decodedJwt?.id)

    if (!client) {
      return res.status(400).json({ error: 'client not found' })
    }

    const seguroAuto = await seguroAutoRepository.findOne({
      where: {
        id: seguroId,
        clientId: client.id
      }
    })

    if (!seguroAuto) {
      return res.status(400).json({ error: 'seguro auto not found' })
    }

    return res.status(200).json(seguroAuto)
  }
}

export default new ClientController()

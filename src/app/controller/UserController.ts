import { Request, Response } from 'express'
import { getRepository } from 'typeorm'
import jwt from 'jsonwebtoken'
import { validate } from 'class-validator'
import User from '@entity/User'
import SeguroAuto from '../entity/SeguroAuto'

interface JwtToken {
  id: number
  iat: number
  exp: number
}

type DecodedJwtToken = JwtToken | null

class UserController {
  async getAll(req: Request, res: Response) {
    const repository = getRepository(User)
    const users = await repository.find()

    users.map(user => delete user.passhash)

    return res.status(200).json(users)
  }

  // ================================================================

  async getUserData(_: Request, res: Response) {
    const userRepository = getRepository(User)

    const token = res.locals.jwtToken

    if (!token) {
      return res.status(400).json({ error: 'Token not provided' })
    }

    const decodedJwt = jwt.decode(token) as DecodedJwtToken

    const user = await userRepository
      .createQueryBuilder('user')
      .where('user.id = :id', { id: decodedJwt?.id })
      .getOne()

    if (!user) {
      return res.status(400).json({ error: 'user not found' })
    }

    return res.status(200).json(user)
  }

  // =================================================================

  async updateUserData(req: Request, res: Response) {
    const userRepository = getRepository(User)
    const { fullName, email } = req.body

    const token = res.locals.jwtToken

    if (!token) {
      return res.status(400).json({ error: 'Token not provided' })
    }

    const decodedJwt = jwt.decode(token) as DecodedJwtToken

    const user = await userRepository.findOne({
      where: {
        id: decodedJwt?.id
      }
    })

    if (!user) {
      return res.status(400).json({ error: 'user not found' })
    }

    if (fullName !== undefined) {
      user.fullName = fullName
    }

    if (email !== undefined) {
      user.email = email
    }

    const userErrors = await validate(user)

    if (userErrors.length >= 1) {
      return res.status(400).json(userErrors)
    }

    try {
      const updatedUser = await userRepository.save(user)
      return res.status(200).json(updatedUser)
    } catch (error) {
      return res.status(400).json(error)
    }
  }

  // =================================================================

  async createSegurosAuto(req: Request, res: Response) {
    const userRepository = getRepository(User)
    const seguroAutoRepository = getRepository(SeguroAuto)

    const {
      clientId,
      veiculo,
      placa,
      seguradora,
      centralDeAtendimento,
      numeroApolice,
      franquia,
      danosMateriais,
      danosCorporais,
      danosMorais,
      carroReserva
    } = req.body

    const token = res.locals.jwtToken

    if (!token) {
      return res.status(400).json({ error: 'Token not provided' })
    }

    const decodedJwt = jwt.decode(token) as DecodedJwtToken

    const user = await userRepository.findOne(decodedJwt?.id)

    if (!user) {
      return res.status(400).json({ error: 'user not found' })
    }

    const seguroAuto = seguroAutoRepository.create({
      clientId,
      veiculo,
      placa,
      seguradora,
      centralDeAtendimento,
      numeroApolice,
      franquia,
      danosMateriais,
      danosCorporais,
      danosMorais,
      carroReserva
    })

    const seguroAutoErros = await validate(seguroAuto)

    if (seguroAutoErros.length >= 1) {
      return res.status(400).json(seguroAutoErros)
    }

    const seguro = await seguroAutoRepository.save(seguroAuto)

    return res.status(200).json(seguro)
  }

  // =================================================================

  async updateSegurosAuto(req: Request, res: Response) {
    const userRepository = getRepository(User)
    const seguroAutoRepository = getRepository(SeguroAuto)

    const { seguroId } = req.params

    const {
      veiculo,
      placa,
      seguradora,
      centralDeAtendimento,
      numeroApolice,
      franquia,
      danosMateriais,
      danosCorporais,
      danosMorais,
      carroReserva
    } = req.body

    const token = res.locals.jwtToken

    if (!token) {
      return res.status(400).json({ error: 'Token not provided' })
    }

    const decodedJwt = jwt.decode(token) as DecodedJwtToken

    const user = await userRepository.findOne(decodedJwt?.id)

    if (!user) {
      return res.status(400).json({ error: 'user not found' })
    }

    const seguroAuto = await seguroAutoRepository.findOne(seguroId)

    if (!seguroAuto) {
      return res.status(400).json({ error: 'seguro auto not found' })
    }

    return res.status(200).json(seguroAuto)
  }

  // =================================================================

  async getSegurosAuto(req: Request, res: Response) {
    const userRepository = getRepository(User)
    const seguroAutoRepository = getRepository(SeguroAuto)

    const token = res.locals.jwtToken

    if (!token) {
      return res.status(400).json({ error: 'Token not provided' })
    }

    const decodedJwt = jwt.decode(token) as DecodedJwtToken

    const user = await userRepository.findOne(decodedJwt?.id)

    if (!user) {
      return res.status(400).json({ error: 'user not found' })
    }

    const seguroAuto = await seguroAutoRepository.find({
      relations: ['client']
    })

    return res.status(200).json(seguroAuto)
  }

  // =================================================================

  async getSegurosAutoById(req: Request, res: Response) {
    const userRepository = getRepository(User)
    const seguroAutoRepository = getRepository(SeguroAuto)
    const { seguroId } = req.params

    const token = res.locals.jwtToken

    if (!seguroId) {
      return res.status(400).json({ error: 'seguroId not provided' })
    }

    if (!token) {
      return res.status(400).json({ error: 'Token not provided' })
    }

    const decodedJwt = jwt.decode(token) as DecodedJwtToken

    const user = await userRepository.findOne(decodedJwt?.id)

    if (!user) {
      return res.status(400).json({ error: 'user not found' })
    }

    const seguroAuto = await seguroAutoRepository.findOne(seguroId, {
      relations: ['client']
    })

    if (!seguroAuto) {
      return res.status(400).json({ error: 'seguro auto not found' })
    }

    return res.status(200).json(seguroAuto)
  }
}

export default new UserController()

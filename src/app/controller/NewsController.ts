import { Request, Response } from 'express'
import { getRepository } from 'typeorm'
import jwt from 'jsonwebtoken'
import { validate } from 'class-validator'
import User from '@entity/User'
import News from '../entity/News'

interface JwtToken {
  id: number
  iat: number
  exp: number
}

type DecodedJwtToken = JwtToken | null

class NewsController {
  async getAll(req: Request, res: Response) {
    const repository = getRepository(News)
    const news = await repository.find()

    return res.status(200).json(news)
  }

  // ==============================================

  async getById(req: Request, res: Response) {
    const repository = getRepository(News)
    const { newsId } = req.params

    if (!newsId) {
      return res.status(400).json({ code: 400, error: 'newsId not provided' })
    }

    const news = await repository.findOne(newsId)

    if (!news) {
      return res.status(400).json({ code: 400, error: 'news not found' })
    }

    return res.status(200).json(news)
  }

  // ==============================================

  async create(req: Request, res: Response) {
    const newsRepository = getRepository(News)
    const userRepository = getRepository(News)

    const { userId, title, content, image, cta } = req.body

    if (!userId) {
      return res.status(400).json({ code: 400, error: 'userId not provided' })
    }

    if (!title) {
      return res.status(400).json({ code: 400, error: 'title not provided' })
    }

    if (!content) {
      return res.status(400).json({ code: 400, error: 'content not provided' })
    }

    if (!image) {
      return res.status(400).json({ code: 400, error: 'image not provided' })
    }

    if (!cta) {
      return res.status(400).json({ code: 400, error: 'cta not provided' })
    }

    const author = await userRepository.findOne(userId)

    if (!author) {
      return res.status(400).json({ code: 400, error: 'user not found' })
    }

    const news = newsRepository.create({
      userId: author.id,
      title,
      content,
      image,
      cta
    })

    const newsErrors = await validate(news)

    if (newsErrors.length >= 1) {
      return res.status(400).json({ code: 400, error: 'invalid fields' })
    }

    try {
      const saved = await newsRepository.save(news)
      return res.status(200).json({ news: saved })
    } catch (error) {
      return res.status(400).json({ code: 400, error: 'error saving news' })
    }
  }
}

export default new NewsController()
